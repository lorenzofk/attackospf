/*-------------------------------------------------------------*/
/* Exemplo Socket Raw - envio de mensagens com struct          */
/*-------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>

#define ETHERTYPE_LEN 2
#define MAC_ADDR_LEN 6
#define ETHERTYPE 0x800
#define BUFFER_SIZE 1024

struct	ospf_hello {
	__u32	oh_netmask;	/* Network Mask			*/
	__u16	oh_hintv;	/* Hello Interval (seconds)	*/
	__u8	oh_opts;	/* Options			*/
	__u8	oh_prio;	/* Sender's Router Priority	*/
	__u32	oh_rdintv;	/* Seconds Before Declare Dead	*/
	__u32	oh_drid;	/* Designated Router ID		*/
	__u32	oh_brid;	/* Backup Designated Router ID	*/
	__u32 oh_neighbor;	/* Living Neighbors		*/
};

struct ospf {
	unsigned char	ospf_version;	/* Version Number		*/
	unsigned char	ospf_type;	/* Packet Type			*/
	unsigned short	ospf_len;	/* Packet Length		*/
	u_int32_t	ospf_rid;	/* Router Identifier		*/
	u_int32_t	ospf_aid;	/* Area Identifier		*/
	unsigned short	ospf_cksum;	/* Check Sum			*/
	unsigned short	ospf_authtype;	/* Authentication Type		*/
	unsigned char	*ospf_data;
	struct ospf_hello hello_pkt;
};

struct lsa_header {
	unsigned short lsa_age;
	unsigned char lsa_options;
	unsigned char lsa_type;
	u_int32_t lsa_link_state_id;
	u_int32_t lsa_advertising_router;
	u_int32_t lsa_sequence_number;
	unsigned short lsa_checksum;
	unsigned short lsa_lengh;
};

struct lls_datablock {
	unsigned short lls_checksum;
	unsigned short lls_data_lenght;
	unsigned short lls_type;
	unsigned short lls_option_lenght;
	u_int32_t lls_option;
};



struct db_descriptor {
	unsigned short db_mtu;
	unsigned char db_options;
	unsigned char db_flags;
	u_int32_t db_ddsequence;

	struct lsa_header lsa;
};


//Função que realiza o cálculo de checksum
unsigned short calcsum(unsigned short *buffer, int length) {
	unsigned long sum; 	

	// initialize sum to zero and loop until length (in words) is 0 
	for (sum=0; length>1; length-=2) // sizeof() returns number of bytes, we're interested in number of words 
		sum += *buffer++;	// add 1 word of buffer to sum and proceed to the next 

	// we may have an extra byte 
	if (length==1)
		sum += (char)*buffer;

	sum = (sum >> 16) + (sum & 0xFFFF);  // add high 16 to low 16 
	sum += (sum >> 16);		     // add carry 
	return ~sum;
}




int main(int argc,char *argv[])
{
	int sock, i;
	int tx_len = 0;
	struct ifreq ifr;
	struct sockaddr_ll to;
	char *src_addr = "192.168.3.12";
	char *dst_addr = "224.0.0.5"; 	
	unsigned char buffer[BUFFER_SIZE];

	socklen_t len;
	
	struct ether_header *eth = (struct ether_header *) buffer;
	struct iphdr *ip_header = (struct iphdr *) (buffer + sizeof(struct ether_header));
	struct ospf *ospf_header = (struct ospf *) (buffer + sizeof(struct ether_header) + sizeof(struct iphdr));
	struct lls_datablock *lls_db = (struct lls_datablock *) (buffer + sizeof(struct ether_header) + sizeof(struct iphdr) + sizeof(struct ospf));
	
	//For the database descriptor
	struct ospf *ospf_header2 = (struct ospf *) (buffer + sizeof(struct ether_header) + sizeof(struct iphdr));
	
    /* Inicializa com 0 os bytes de memoria apontados por ifr. */
	memset(&ifr, 0, sizeof(ifr));
	
	if((sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
		printf("Erro na criacao do socket.\n");
		exit(1);
	}

	//Identicacao de qual maquina (MAC) deve receber a mensagem enviada no socket.
	to.sll_family = htons(PF_PACKET);
	to.sll_protocol = htons(ETH_P_ALL);
	to.sll_halen = 6;
	to.sll_ifindex = 2;  //Índice da interface pela qual os pacotes serao enviados. 2 = eth0, 3 = wlan0


	memset(buffer, 0, BUFFER_SIZE);
	
	eth->ether_dhost[0] = 0x01;
	eth->ether_dhost[1] = 0x00;
	eth->ether_dhost[2] = 0x5e;
	eth->ether_dhost[3] = 0x00;
	eth->ether_dhost[4] = 0x00;
	eth->ether_dhost[5] = 0x00;
	
	
	eth->ether_shost[0] = 0x78;
	eth->ether_shost[1] = 0x2b;
	eth->ether_shost[2] = 0xcb;
	eth->ether_shost[3] = 0xec;
	eth->ether_shost[4] = 0x8c;
	eth->ether_shost[5] = 0xd4;

	eth->ether_type = htons(ETH_P_IP);

	tx_len += sizeof(struct ether_header);

	//Definimos o cabeçalho IP
	ip_header->ihl = 5;
	ip_header->tos = 0xc0;
	ip_header->version = 4;
	ip_header->tot_len = htons(sizeof(struct iphdr) + sizeof(struct ospf) + sizeof(struct lls_datablock));
	ip_header->id = 0;
	ip_header->frag_off = 0;
	ip_header->ttl = 1;
	ip_header->protocol = 89;
	ip_header->saddr = inet_addr(src_addr);
	ip_header->daddr = inet_addr(dst_addr);
	ip_header->check = calcsum((unsigned short *) ip_header, sizeof(struct iphdr));

	tx_len += sizeof(struct iphdr);

	
	unsigned char authData[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
	//Definimos o cabeçalho comum do OSPF
	ospf_header->ospf_version = 2;
	ospf_header->ospf_type = 1;
	ospf_header->ospf_len = htons(0x0030);
	ospf_header->ospf_rid = inet_addr("192.168.3.12"); //Veneno
	ospf_header->ospf_aid = 0;
	
	ospf_header->hello_pkt.oh_netmask = inet_addr("255.255.255.0");
	ospf_header->hello_pkt.oh_hintv = htons(0x000a);
	ospf_header->hello_pkt.oh_opts = 18;
	ospf_header->hello_pkt.oh_prio = 0x01;
	ospf_header->hello_pkt.oh_rdintv = inet_addr("0.0.0.40"); //Gambiarra pra setar 40 segundos
	ospf_header->hello_pkt.oh_drid = inet_addr("192.168.3.1");
	ospf_header->hello_pkt.oh_brid = inet_addr("0.0.0.0");
	ospf_header->hello_pkt.oh_neighbor = inet_addr("192.168.3.1");

	ospf_header->ospf_cksum = calcsum((unsigned short *) ospf_header, sizeof(struct ospf));
	
	ospf_header->ospf_authtype = 0;
	ospf_header->ospf_data = &authData[0];
	
	tx_len += sizeof(struct ospf);

	lls_db->lls_checksum = 0xf6ff; 
	lls_db->lls_data_lenght = 0x0300; 
	lls_db->lls_type = 0x0100; 
	lls_db->lls_option_lenght = 0x0400;
	lls_db->lls_option = inet_addr("0.0.0.1"); 
	
	tx_len += sizeof(struct lls_datablock);
	

	//A partir daqui é o db descriptor
	memset(buffer, 0, BUFFER_SIZE);
	
	eth->ether_dhost[0] = 0x01;
	eth->ether_dhost[1] = 0x00;
	eth->ether_dhost[2] = 0x5e;
	eth->ether_dhost[3] = 0x00;
	eth->ether_dhost[4] = 0x00;
	eth->ether_dhost[5] = 0x00;
	
	eth->ether_shost[0] = 0x78;
	eth->ether_shost[1] = 0x2b;
	eth->ether_shost[2] = 0xcb;
	eth->ether_shost[3] = 0xec;
	eth->ether_shost[4] = 0x8c;
	eth->ether_shost[5] = 0xd4;

	eth->ether_type = htons(ETH_P_IP);

	tx_len += sizeof(struct ether_header);

	//Definimos o cabeçalho IP
	ip_header->ihl = 5;
	ip_header->tos = 0xc0;
	ip_header->version = 4;
	ip_header->tot_len = htons(sizeof(struct iphdr) + sizeof(struct ospf) + sizeof(struct lls_datablock));
	ip_header->id = 0;
	ip_header->frag_off = 0;
	ip_header->ttl = 1;
	ip_header->protocol = 89;
	ip_header->saddr = inet_addr(src_addr);
	ip_header->daddr = inet_addr(dst_addr);
	ip_header->check = calcsum((unsigned short *) ip_header, sizeof(struct iphdr));

	tx_len += sizeof(struct iphdr);

	//Definimos o cabeçalho comum do OSPF
	ospf_header2->ospf_version = 2;
	ospf_header2->ospf_type = 2;
	ospf_header2->ospf_len = htons(0x0030);
	ospf_header2->ospf_rid = inet_addr("192.168.3.12"); //Veneno
	ospf_header2->ospf_aid = 0;
	ospf_header2->ospf_cksum = 0x00;
	
	ospf_header2->ospf_authtype = 0;
	ospf_header2->ospf_data = &authData[0];
	

	if(sendto(sock, buffer, tx_len, 0, (struct sockaddr *)&(to), sizeof(struct sockaddr_ll)) < 0) {
		printf("ERROR! sendto() \n");
		exit(1);
	}
	else {
		printf("Send to target machine.\n");
	}
}

