/*-------------------------------------------------------------*/
/* Exemplo Socket Raw - envio de mensagens com struct          */
/*-------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <time.h>
//For checksum of LSU

#define ETHERTYPE_LEN 2
#define MAC_ADDR_LEN 6
#define ETHERTYPE 0x800
#define BUFFER_SIZE 1024

const int MODX = 4102;

struct    ospf_hello {
    __u32    oh_netmask;    /* Network Mask            */
    __u16    oh_hintv;    /* Hello Interval (seconds)    */
    __u8    oh_opts;    /* Options            */
    __u8    oh_prio;    /* Sender's Router Priority    */
    __u32    oh_rdintv;    /* Seconds Before Declare Dead    */
    __u32    oh_drid;    /* Designated Router ID        */
    __u32    oh_brid;    /* Backup Designated Router ID    */
    __u32 oh_neighbor;    /* Living Neighbors        */
};


struct ospf {
    unsigned char    ospf_version;    /* Version Number        */
    unsigned char    ospf_type;    /* Packet Type            */
    unsigned short    ospf_len;    /* Packet Length        */
    u_int32_t    ospf_rid;    /* Router Identifier        */
    u_int32_t    ospf_aid;    /* Area Identifier        */
    unsigned short    ospf_cksum;    /* Check Sum            */
    unsigned short    ospf_authtype;    /* Authentication Type        */
    unsigned char    *ospf_data;
    struct ospf_hello hello_pkt;
};

struct	ospf_dd {
	__u16	dd_mbz;		/* Must Be Zero			*/
	__u8	dd_opts;	/* Options			*/
	__u8	dd_control;	/* Control Bits	(DDC_* below)	*/
	__u32	dd_seq;		/* Sequence Number		*/
};

struct ospf_with_db {
    unsigned char    ospf_version;    /* Version Number        */
    unsigned char    ospf_type;    /* Packet Type            */
    unsigned short    ospf_len;    /* Packet Length        */
    u_int32_t    ospf_rid;    /* Router Identifier        */
    u_int32_t    ospf_aid;    /* Area Identifier        */
    unsigned short    ospf_cksum;    /* Check Sum            */
    unsigned short    ospf_authtype;    /* Authentication Type        */
    unsigned char    *ospf_data;
    struct ospf_dd dd_pkt;
};

struct lls_datablock {
    unsigned short lls_checksum;
    unsigned short lls_data_lenght;
    unsigned short lls_type;
    unsigned short lls_option_lenght;
    u_int32_t lls_option;
};

struct ospf_lsu {
	__u32 lsu_number; //4
	__u16 lsu_age; //2 
	__u8 lsu_options; //1
	__u8 lsu_type; //1
	__u32 lsu_link_state_id; //4
	__u32 lsu_advertising_router; //4
	__u32 lsu_sequence_number; //4
	__u16 lsu_checksum;	//2
	__u16 lsu_lenght; //2
	__u32 lsu_net_mask; //4
	__u32 lsu_attached_router;
	__u32 lsu_attached_router1;
};



//Função que realiza o cálculo de checksum
unsigned short calcsum(unsigned short *buffer, int length) {
    unsigned long sum;     

    // initialize sum to zero and loop until length (in words) is 0 
    for (sum=0; length>1; length-=2) // sizeof() returns number of bytes, we're interested in number of words 
        sum += *buffer++;    // add 1 word of buffer to sum and proceed to the next 

    // we may have an extra byte 
    if (length==1)
        sum += (char)*buffer;

    sum = (sum >> 16) + (sum & 0xFFFF);  // add high 16 to low 16 
    sum += (sum >> 16);             // add carry 
    return ~sum;
}

__u16 fletcher(__u8 *message, int mlen, int offset)   
   
{   
    __u8 *ptr;   
    __u8 *end;   
    int c0; // Checksum high byte   
    int c1; // Checksum low byte   
    __u16 cksum;    // Concatenated checksum   
    int iq; // Adjust for message placement, high byte   
    int ir; // low byte   
   
    // Set checksum field to zero   
    if (offset) {   
    message[offset-1] = 0;   
    message[offset] = 0;   
    }   
   
    // Initialize checksum fields   
    c0 = 0;   
    c1 = 0;   
    ptr = message;   
    end = message + mlen;   
   
    // Accumulate checksum   
    while (ptr < end) {   
    __u8    *stop;   
    stop = ptr + MODX;   
    if (stop > end)   
        stop = end;   
    for (; ptr < stop; ptr++) {   
        c0 += *ptr;   
        c1 += c0;   
    }   
    // Ones complement arithmetic   
    c0 = c0 % 255;   
    c1 = c1 % 255;   
    }   
   
    // Form 16-bit result   
    cksum = (c1 << 8) + c0;   
   
    // Calculate and insert checksum field   
    if (offset) {   
    iq = ((mlen - offset)*c0 - c1) % 255;   
    if (iq <= 0)   
        iq += 255;   
    ir = (510 - c0 - iq);   
    if (ir > 255)   
        ir -= 255;   
    message[offset-1] = iq;   
    message[offset] = ir;   
    }   
   
    return(cksum);   
}


void sendHello() {
	int sock, i;
    int tx_len = 0;
    struct ifreq ifr;
    struct sockaddr_ll to;
    char *src_addr = "192.168.3.12";
    char *dst_addr = "224.0.0.5";     
    unsigned char buffer[1024];

    socklen_t len;
    
    struct ether_header *eth = (struct ether_header *) buffer;
    struct iphdr *ip_header = (struct iphdr *) (buffer + sizeof(struct ether_header));
    struct ospf *ospf_header = (struct ospf *) (buffer + sizeof(struct ether_header) + sizeof(struct iphdr));
    struct lls_datablock *lls_db = (struct lls_datablock *) (buffer + sizeof(struct ether_header) + sizeof(struct iphdr) + sizeof(struct ospf));
    
    /* Inicializa com 0 os bytes de memoria apontados por ifr. */
    memset(&ifr, 0, sizeof(ifr));
    
    if((sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
        printf("Erro na criacao do socket.\n");
        exit(1);
    }

    //Identicacao de qual maquina (MAC) deve receber a mensagem enviada no socket.
    to.sll_family = htons(PF_PACKET);
    to.sll_protocol = htons(ETH_P_ALL);
    to.sll_halen = 6;
    to.sll_ifindex = 2;  //Índice da interface pela qual os pacotes serao enviados. 2 = eth0, 3 = wlan0


    memset(buffer, 0, BUFFER_SIZE);
    
    eth->ether_dhost[0] = 0x01;
    eth->ether_dhost[1] = 0x00;
    eth->ether_dhost[2] = 0x5e;
    eth->ether_dhost[3] = 0x00;
    eth->ether_dhost[4] = 0x00;
    eth->ether_dhost[5] = 0x00;
    
    
    eth->ether_shost[0] = 0x78;
    eth->ether_shost[1] = 0x2b;
    eth->ether_shost[2] = 0xcb;
    eth->ether_shost[3] = 0xec;
    eth->ether_shost[4] = 0x8c;
    eth->ether_shost[5] = 0xd4;

    eth->ether_type = htons(ETH_P_IP);

    tx_len += sizeof(struct ether_header);

    //Definimos o cabeçalho IP
    ip_header->ihl = 5;
    ip_header->tos = 0xc0;
    ip_header->version = 4;
    ip_header->tot_len = htons((sizeof(struct iphdr) + sizeof(struct ospf) + sizeof(struct lls_datablock)));
    ip_header->id = 0;
    ip_header->frag_off = 0;
    ip_header->ttl = 1;
    ip_header->protocol = 89;
    ip_header->saddr = inet_addr(src_addr);
    ip_header->daddr = inet_addr(dst_addr);
    ip_header->check = calcsum((unsigned short *) ip_header, sizeof(struct iphdr));

    tx_len += sizeof(struct iphdr);

    
    unsigned char authData[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
    //Definimos o cabeçalho comum do OSPF
    ospf_header->ospf_version = 2;
    ospf_header->ospf_type = 1;
    ospf_header->ospf_len = htons(0x0030);
    ospf_header->ospf_rid = inet_addr("192.168.3.12"); //Veneno
    ospf_header->ospf_aid = 0;
    
    ospf_header->hello_pkt.oh_netmask = inet_addr("255.255.255.0");
    ospf_header->hello_pkt.oh_hintv = htons(0x000a);
    ospf_header->hello_pkt.oh_opts = 18;
    ospf_header->hello_pkt.oh_prio = 0x01;
    ospf_header->hello_pkt.oh_rdintv = inet_addr("0.0.0.40"); //Gambiarra pra setar 40 segundos
    ospf_header->hello_pkt.oh_drid = inet_addr("192.168.3.1");
    ospf_header->hello_pkt.oh_brid = inet_addr("0.0.0.0");
    ospf_header->hello_pkt.oh_neighbor = inet_addr("192.168.3.1");

    ospf_header->ospf_cksum = calcsum((unsigned short *) ospf_header, sizeof(struct ospf));
    
    ospf_header->ospf_authtype = 0;
    ospf_header->ospf_data = &authData[0];
    
    tx_len += sizeof(struct ospf);

    lls_db->lls_checksum = 0xf6ff; 
    lls_db->lls_data_lenght = 0x0300; 
    lls_db->lls_type = 0x0100; 
    lls_db->lls_option_lenght = 0x0400;
    lls_db->lls_option = inet_addr("0.0.0.1"); 
    
    tx_len += sizeof(struct lls_datablock);
    
	if(sendto(sock, buffer, tx_len, 0, (struct sockaddr *)&(to), sizeof(struct sockaddr_ll)) < 0) {
        printf("ERROR! sendto() \n");
        exit(1);
    }
    else {
        printf("HELLO PACKET HAS BEEN SEND.\n");
    }
}

void sendDbDescription(int sequence, int control) {
	int sock, i;
    int tx_len = 0;
    struct ifreq ifr;
    struct sockaddr_ll to;
    char *src_addr = "192.168.3.12";
    char *dst_addr = "192.168.3.1";     
    unsigned char buffer[BUFFER_SIZE];

    socklen_t len;
    
    struct ether_header *eth = (struct ether_header *) buffer;
    struct iphdr *ip_header = (struct iphdr *) (buffer + sizeof(struct ether_header));
    struct ospf_with_db *ospf_header = (struct ospf_with_db *) (buffer + sizeof(struct ether_header) + sizeof(struct iphdr));
   	struct lls_datablock *lls_db = (struct lls_datablock *) (buffer + sizeof(struct ether_header) + sizeof(struct iphdr) + (sizeof(struct ospf_with_db)));
    
    /* Inicializa com 0 os bytes de memoria apontados por ifr. */
    memset(&ifr, 0, sizeof(ifr));
    
    if((sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
        printf("Erro na criacao do socket.\n");
        exit(1);
    }

    //Identicacao de qual maquina (MAC) deve receber a mensagem enviada no socket.
    to.sll_family = htons(PF_PACKET);
    to.sll_protocol = htons(ETH_P_ALL);
    to.sll_halen = 6;
    to.sll_ifindex = 2;  //Índice da interface pela qual os pacotes serao enviados. 2 = eth0, 3 = wlan0


    memset(buffer, 0, BUFFER_SIZE);
    
    eth->ether_dhost[0] = 0x50;
    eth->ether_dhost[1] = 0x3d;
    eth->ether_dhost[2] = 0xe5;
    eth->ether_dhost[3] = 0xd9;
    eth->ether_dhost[4] = 0x07;
    eth->ether_dhost[5] = 0xb8;
    
    
    eth->ether_shost[0] = 0x78;
    eth->ether_shost[1] = 0x2b;
    eth->ether_shost[2] = 0xcb;
    eth->ether_shost[3] = 0xec;
    eth->ether_shost[4] = 0x8c;
    eth->ether_shost[5] = 0xd4;

    eth->ether_type = htons(ETH_P_IP);

    tx_len += sizeof(struct ether_header);

    //Definimos o cabeçalho IP
    ip_header->ihl = 5;
    ip_header->tos = 0xc0;
    ip_header->version = 4;
    ip_header->tot_len = htons(sizeof(struct iphdr) + sizeof(struct ospf_with_db) + sizeof(struct lls_datablock));
    ip_header->id = 0;
    ip_header->frag_off = 0;
    ip_header->ttl = 1;
    ip_header->protocol = 89;
    ip_header->saddr = inet_addr(src_addr);
    ip_header->daddr = inet_addr(dst_addr);
    ip_header->check = calcsum((unsigned short *) ip_header, sizeof(struct iphdr));

    tx_len += sizeof(struct iphdr);

    unsigned char authData[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
    //Definimos o cabeçalho comum do OSPF
    ospf_header->ospf_version = 2;
    ospf_header->ospf_type = 2;
    ospf_header->ospf_len = htons(0x0020);
    ospf_header->ospf_rid = inet_addr("192.168.3.12"); //Veneno
    ospf_header->ospf_aid = 0;  
	
	ospf_header->dd_pkt.dd_mbz = 0xdc05;
	ospf_header->dd_pkt.dd_opts = 0x52;
	ospf_header->dd_pkt.dd_control = control;
	ospf_header->dd_pkt.dd_seq = htonl(sequence);

    ospf_header->ospf_cksum = calcsum((unsigned short *) ospf_header, sizeof(struct ospf_with_db));
    
	ospf_header->ospf_authtype = 0;
    ospf_header->ospf_data = 0;
	
	tx_len += (sizeof(struct ospf_with_db));

	lls_db->lls_checksum = 0xf6ff; 
    lls_db->lls_data_lenght = 0x0300; 
    lls_db->lls_type = 0x0100; 
    lls_db->lls_option_lenght = 0x0400;
    lls_db->lls_option = inet_addr("0.0.0.1"); 
    
    tx_len += sizeof(struct lls_datablock);
	
	if(sendto(sock, buffer, tx_len, 0, (struct sockaddr *)&(to), sizeof(struct sockaddr_ll)) < 0) {
        printf("ERROR! sendto() \n");
        exit(1);
    }
    else {
        printf("DB DESCRIPTION HAS BEEN SEND.\n");
    }    
	
}


void sendLsUpdate(int sequence, int age) {
	int sock, i;
    int tx_len = 0;
    struct ifreq ifr;
    struct sockaddr_ll to;
    char *src_addr = "192.168.3.12";
    char *dst_addr = "192.168.3.1";     
    unsigned char buffer[BUFFER_SIZE];

    socklen_t len;
    
    struct ether_header *eth = (struct ether_header *) buffer;
    struct iphdr *ip_header = (struct iphdr *) (buffer + sizeof(struct ether_header));
    struct ospf_with_db *ospf_header = (struct ospf_with_db *) (buffer + sizeof(struct ether_header) + sizeof(struct iphdr));
   	struct ospf_lsu *lsu = (struct ospf_lsu *) (buffer + sizeof(struct ether_header) + sizeof(struct iphdr) + (sizeof(struct ospf_with_db) - 8));
    
    /* Inicializa com 0 os bytes de memoria apontados por ifr. */
    memset(&ifr, 0, sizeof(ifr));
    
    if((sock = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
        printf("Erro na criacao do socket.\n");
        exit(1);
    }

    //Identicacao de qual maquina (MAC) deve receber a mensagem enviada no socket.
    to.sll_family = htons(PF_PACKET);
    to.sll_protocol = htons(ETH_P_ALL);
    to.sll_halen = 6;
    to.sll_ifindex = 2;  //Índice da interface pela qual os pacotes serao enviados. 2 = eth0, 3 = wlan0


    memset(buffer, 0, BUFFER_SIZE);
    
    eth->ether_dhost[0] = 0x50;
    eth->ether_dhost[1] = 0x3d;
    eth->ether_dhost[2] = 0xe5;
    eth->ether_dhost[3] = 0xd9;
    eth->ether_dhost[4] = 0x07;
    eth->ether_dhost[5] = 0xb8;
    
    
    eth->ether_shost[0] = 0x78;
    eth->ether_shost[1] = 0x2b;
    eth->ether_shost[2] = 0xcb;
    eth->ether_shost[3] = 0xec;
    eth->ether_shost[4] = 0x8c;
    eth->ether_shost[5] = 0xd4;

    eth->ether_type = htons(ETH_P_IP);

    tx_len += sizeof(struct ether_header);

    //Definimos o cabeçalho IP
    ip_header->ihl = 5;
    ip_header->tos = 0xc0;
    ip_header->version = 4;
    ip_header->tot_len = htons(sizeof(struct iphdr) + sizeof(struct ospf_with_db) + sizeof(struct ospf_lsu) - 8);
    ip_header->id = 0;
    ip_header->frag_off = 0;
    ip_header->ttl = 1;
    ip_header->protocol = 89;
    ip_header->saddr = inet_addr(src_addr);
    ip_header->daddr = inet_addr(dst_addr);
    ip_header->check = calcsum((unsigned short *) ip_header, sizeof(struct iphdr));

    tx_len += sizeof(struct iphdr);

    //Definimos o cabeçalho comum do OSPF
    ospf_header->ospf_version = 2;
    ospf_header->ospf_type = 4;
    ospf_header->ospf_len = htons(0x0020);
    ospf_header->ospf_rid = inet_addr("192.168.3.12"); //Veneno
    ospf_header->ospf_aid = 0;  
	
	ospf_header->ospf_authtype = 0;
    ospf_header->ospf_data = 0;
	
	
	tx_len += (sizeof(struct ospf_with_db) - 8);

	//0x17e6

	lsu->lsu_number = inet_addr("1");
	lsu->lsu_age = htons(age);
	lsu->lsu_options = 0x22;
	lsu->lsu_type = 0x02;
	lsu->lsu_link_state_id = inet_addr("192.168.3.12");
	lsu->lsu_advertising_router = inet_addr("192.168.3.12");
	lsu->lsu_sequence_number = htonl(sequence);
	lsu->lsu_lenght = htons(sizeof(struct ospf_lsu) - 4);
	lsu->lsu_checksum = 0x00;
	lsu->lsu_net_mask = inet_addr("255.255.255.0");
	lsu->lsu_attached_router = inet_addr("192.168.3.1");
	lsu->lsu_attached_router1 = inet_addr("192.168.3.12");
	
	fletcher((uint8_t *) lsu + 6, sizeof(struct ospf_lsu) - 6, 15);
	
	

	tx_len += (sizeof(struct ospf_lsu));

	ospf_header->ospf_cksum = calcsum((unsigned short *) ospf_header, (sizeof(struct ospf_with_db) - 8) + sizeof(struct ospf_lsu));
    

	if(sendto(sock, buffer, tx_len, 0, (struct sockaddr *)&(to), sizeof(struct sockaddr_ll)) < 0) {
        printf("ERROR! sendto() \n");
        exit(1);
    }
    else {
        printf("LS UPDATE HAS BEEN SEND.\n");
    }    
	
}

int main(int argc,char *argv[])
{
	int i;
	
	sendHello();
	sleep(2);
	sendDbDescription(10000, 7);
	sleep(2);
	sendDbDescription(10001, 3);
	sleep(2);
	sendDbDescription(10002, 1);
	sleep(2);
	
	int j = 10003;
	for(i = 5; i <= 5; i += 5) {
		sendLsUpdate(j, i);
		j += 1;
	}
}

